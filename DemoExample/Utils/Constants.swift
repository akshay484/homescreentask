//
//  Constants.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 30/08/21.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire

//MARK: -  Storyboards

struct Storyboard {
    static var launchScreen: UIStoryboard {
        return UIStoryboard(name: "LaunchScreen", bundle: Bundle.main)
    }
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
}

//MARK: - API Endpoints, Base URL & API Keys
struct API{
    static let baseURL = "https://api.npoint.io/"
    static let homeData = "342e5cd515f30ecb6e90"
}

//MARK: - error message values
struct ErrorMessages{
    static let APIError = "Something went bad!"
    static let APIAlertTitle = "Alert"
    static let noInternetTitle = "No Internet Connection"
    static let noInternetMessage = "Please check your internet connection."
}

//MARK: - font values
struct Fonts{
    static let openSansSemibold =  "OpenSans-SemiBold"
    static let openSansRegular =  "OpenSans-Regular"
}


//MARK: - color code values
struct ColorCodes{
    static let textPrimaryColor =  "#ffffff"
    static let mainThemeColor =  "#8c2893"
}



//MARK: - corner radius values
struct CornerRadius{
    static let standard = 16
}


//MARK:- cell identifiers for table and collection views
struct CellIdentifiers{
    static let homeCollectionViewCell = "HomeCollectionViewCell"
}


/* Create request header */
func getHeader() -> HTTPHeaders {
    var header = HTTPHeaders()
    header["Accept"] = "application/json"
    header["Content-Type"] = "application/json"
    return header
}


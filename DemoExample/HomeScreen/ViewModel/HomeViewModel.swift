//
//  HomeViewModel.swift
//  DemoMovie
//
//  Created by Aditya_mac_4 on 01/09/21.
//

import Foundation
import ObjectMapper

//MARK: -  HomeViewModel delegate to get success/failure from API

protocol HomeViewModelDelegate {
    func didFetchHomeData(responseData: Any)
    func didFailFetchHomeData(error: NSError)
}

class HomeViewModel:Networking,MainServiceDelegate{
    
    
    var homeViewModelDelegate: HomeViewModelDelegate?
    var currentAPI = ""
    
    
    //MARK: - if we get success from the Networking class methods then this method is called.
    func didFetchData(responseData: Any) {
        if(currentAPI == API.homeData){
            self.homeViewModelDelegate?.didFetchHomeData(responseData: responseData)
        }
    }
    
    //MARK: - if we get failure from the Networking class methods then this method is called.
    func didFailWithError(error: NSError) {
        if(currentAPI == API.homeData){
            self.homeViewModelDelegate?.didFailFetchHomeData(error: error)
        }
    }
    
    
    //MARK:-  Service call for getting trending movies for Home data
    func getHomeDataFromServer(methodName: String , delegate:HomeViewModelDelegate) -> Void {
        self.currentAPI = methodName
        self.homeViewModelDelegate = delegate
        super.mainServerdelegate = self
        let url = API.baseURL + methodName
        super.getCallWithAlamofire(serverUrl: url)
    }
    
    //MARK:-  Parsing Home data from JSON
    func parseHomeListData(completion: @escaping ((Bool , String , String , HomeModel?) -> Void) , responseData:Any) {
        if let responseDict = responseData as? NSDictionary {
            let jsonData = Mapper<HomeModel>().map(JSONObject: responseDict)
            if(jsonData?.status ?? "" == "200"){
                completion(true , "" , ErrorMessages.APIAlertTitle,jsonData)
            }else{
                completion(false ,ErrorMessages.APIError, ErrorMessages.APIAlertTitle,nil)
            }
        } else {
            completion(false, ErrorMessages.APIError , ErrorMessages.APIAlertTitle,nil)
        }
    }
    
    
    
    
}

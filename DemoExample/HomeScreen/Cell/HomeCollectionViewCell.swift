//
//  HomeCollectionViewCell.swift
//  DemoExample
//
//  Created by Aditya_mac_4 on 22/09/21.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItem: UILabel!
    @IBOutlet weak var viewItemBack: UIView!
    
}

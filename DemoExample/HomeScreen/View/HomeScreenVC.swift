//
//  ViewController.swift
//  DemoExample
//
//  Created by Aditya_mac_4 on 22/09/21.
//

import UIKit
import SDWebImage

class HomeScreenVC: AppMainVc {
    
    
    @IBOutlet weak var viewNavBar: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var colHomeData: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var heightSearchBar: NSLayoutConstraint!
    
    lazy var homeViewModel = HomeViewModel()
    var homeModel: HomeModel?{
        didSet{
            DispatchQueue.main.async {
                self.primaryCategoryData = self.homeModel?.data?.categoriesData ?? []
                self.filteredCategoryData = self.homeModel?.data?.categoriesData ?? []
                self.lblUserName.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textPrimaryColor, fontSize: 13, text: "Hello, \(self.homeModel?.data?.navigationData?.name ?? "")")
                self.lblAddress.setTextStyles(font: Fonts.openSansRegular, fontColor: ColorCodes.textPrimaryColor, fontSize: 11, text: self.homeModel?.data?.navigationData?.address ?? "")
            }
        }
    }
    
    var primaryCategoryData = [CategoriesDataModel](){
        didSet{
            DispatchQueue.main.async {
                self.colHomeData.reloadData()
            }
        }
    }
    
    var filteredCategoryData = [CategoriesDataModel](){
        didSet{
            DispatchQueue.main.async {
                self.colHomeData.reloadData()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUISettings()
        self.colHomeData.delegate = self
        self.colHomeData.dataSource = self
        self.searchBar.delegate = self
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        self.colHomeData?.collectionViewLayout = layout
        
        self.homeViewModel.getHomeDataFromServer(methodName: API.homeData, delegate: self)
    }
    
    
    //MARK:- setup UI settings
    func setupUISettings(){
        self.viewNavBar.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
        self.heightSearchBar.constant = 0
    }
    
    
    @IBAction func didSelectSearch(_ sender: UIButton){
        if(self.heightSearchBar.constant == 51){
            self.heightSearchBar.constant = 0
        }else{
            self.heightSearchBar.constant = 51
        }
    }
    
    
}



//MARK: - UICollectionView Delegate and DataSource
extension HomeScreenVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredCategoryData.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(self.filteredCategoryData.count == 1){
            let numberOfItemsPerRow:CGFloat = 2
            let spacingBetweenCells:CGFloat = 0
            
            let totalSpacing = (2 * 10) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
            
            if let collection = self.colHomeData{
                let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow + 60
                return CGSize(width: width, height: 150)
            }
        }else{
        
        if let categoryData = self.homeModel?.data?.categoriesData?[indexPath.row]{
            if(categoryData.type ?? "" == "big"){
                let numberOfItemsPerRow:CGFloat = 2
                let spacingBetweenCells:CGFloat = 0
                
                let totalSpacing = (2 * 10) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
                
                if let collection = self.colHomeData{
                    let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow + 60
                    return CGSize(width: width, height: 150)
                }
                
            }else if(categoryData.type ?? "" == "small"){
                
                let numberOfItemsPerRow:CGFloat = 2
                let spacingBetweenCells:CGFloat = 0
                
                let totalSpacing = (2 * 10) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
                
                if let collection = self.colHomeData{
                    let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow - 60
                    return CGSize(width: width, height: 150)
                }
                
            }
        }
        }
        
        return CGSize(width: 150, height: 150)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell{
            
            let categoryData = self.filteredCategoryData[indexPath.row]
            cell.lblItem.setTextStyles(font: Fonts.openSansSemibold, fontColor: ColorCodes.textPrimaryColor, fontSize: 10, text: categoryData.title ?? "")
            cell.viewItemBack.backgroundColor = UIColor.init(hexString: ColorCodes.mainThemeColor)
            cell.imgItem.sd_setImage(with: URL.init(string: categoryData.image ?? ""), completed: nil)

            cell.layer.cornerRadius = 8
            cell.layer.masksToBounds = true
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
}



//MARK: - SearchBar Delegate
extension HomeScreenVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if(searchText != ""){
            self.filteredCategoryData = self.primaryCategoryData.filter({ obj in
                obj.title?.lowercased().hasPrefix(searchText.lowercased()) ?? false
//                obj.title?.lowercased().contains(searchText.lowercased()) ?? false
            })
            DispatchQueue.main.async {
                self.colHomeData.reloadData()
            }
        }else{
            self.filteredCategoryData = self.primaryCategoryData
        }
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    
}


//MARK: - API call and methods for parsing
extension HomeScreenVC: HomeViewModelDelegate{
    
    
    func didFetchHomeData(responseData: Any) {
        self.homeViewModel.parseHomeListData(completion: {isSuccess, message, title, homeObj in
            if(isSuccess){
                self.homeModel = homeObj
            }else{
                self.showAlertViewWithTitle(title, message: message)
            }
        }, responseData: responseData)
    }
    
    func didFailFetchHomeData(error: NSError) {
        super.showAlertViewWithTitle(ErrorMessages.APIAlertTitle, message: ErrorMessages.APIError)
    }
    
    
}

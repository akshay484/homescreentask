//
//  HomeDataModel.swift
//  DemoExample
//
//  Created by Aditya_mac_4 on 22/09/21.
//

import Foundation
import ObjectMapper

class HomeModel: Mappable{
    var status: String?
    var message: String?
    var data: HomeDataModel?
    
    required init(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
}


class HomeDataModel: Mappable{
    var navigationData: NavigationDataModel?
    var categoriesData: [CategoriesDataModel]?
    
    required init(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        navigationData <- map["navigationData"]
        categoriesData <- map["categoriesData"]
    }
    
    
}


class NavigationDataModel: Mappable{
    var name: String?
    var address: String?
    
    required init(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        name <- map["name"]
        address <- map["address"]
    }
}

class CategoriesDataModel: Mappable{
    var title: String?
    var image: String?
    var type: String?
    
    required init(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        title <- map["title"]
        image <- map["image"]
        type <- map["type"]
    }
}
